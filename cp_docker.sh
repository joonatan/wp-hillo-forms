# build is the React build dir so it should be inside the plugin
# then we need wordpress dir for the php sources (the main plugin file)
docker exec -it hillopurkki_wordpress_1 rm -r /var/www/html/wp-content/plugins/hillo-calculator/
docker cp wordpress/. hillopurkki_wordpress_1:/var/www/html/wp-content/plugins/hillo-calculator/
docker cp build/static/. hillopurkki_wordpress_1:/var/www/html/wp-content/plugins/hillo-calculator/static/
