<?php
/*
 * Plugin Name: Hillopurkki Calculator
 * Plugin URI:
 * Description: Hillopurkis proffit calculator with contact form.
 * This requires JS since our form is React only without static generator
 *
 * Version: 0.0.2
 * Author: Joonatan Kuosa
 */

/*
 * a shortcode that outputs the React widget
 *
 * TODO
 *  add form validation
 *  add email sending for forms
 */

add_action( 'init', function () {
  add_shortcode('hillo_calculator', 'hillo_calculator');
} );

// high priority to avoid CSS overriding theme CSS
add_action( 'wp_enqueue_scripts', 'hillo_enque_scripts', 1 );
add_action( 'admin_enqueue_scripts', 'hillo_admin_enqueue_react', 1 );

function hillo_admin_enqueue_react( $hook ) {
  if ( 'hillo-forms_page_hillo_settings' != $hook ) {
    return;
  }

  hillo_enque_scripts();
}

function hillo_enque_scripts () {
  // find the CSS and JS bundles
  $fcss = glob(__DIR__ . "/static/css/main.*.css");
  $fjs = glob(__DIR__ . "/static/js/main.*.js");

  if ( count($fcss) > 0 ) {
    $spos = strrpos($fcss[0], "main.");
    $file_str = substr($fcss[0], $spos);
    error_log('CSS file url: ' . plugins_url( "/static/css/{$file_str}", __FILE__ ));

    wp_enqueue_style(
      'hillo-calculator-style',
      plugins_url( "/static/css/{$file_str}", __FILE__ ),
    );
  } else {
    error_log("ERROR: hillo-calculator CSS files missing ");
  }

  if ( count($fjs) > 0 ) {
    $spos = strrpos($fjs[0], "main.");
    $file_str = substr($fjs[0], $spos);
    error_log('CSS file url: ' . plugins_url( "/static/js/{$file_str}", __FILE__ ));

    wp_register_script(
      'hillo-calculator-js',
      plugins_url( "/static/js/{$file_str}", __FILE__ ),
      [],
      false,
      true
    );
    wp_localize_script( 'hillo-calculator-js', 'data', [
        'nonce' => wp_create_nonce( 'wp_rest' ),
    ] );
    wp_enqueue_script( 'hillo-calculator-js' );
  } else {
    error_log("ERROR: hillo-calculator JS files missing ");
  }
}

function hillo_calculator () {
  ob_start();
?>
  <noscript>You need to enable JavaScript to run this app.</noscript>
  <div id="hillo-calculator"></div>
<?php
  return ob_get_clean();
}

/*"***************************** Admin ***************************************/
add_action( 'admin_menu', function () {
  add_menu_page(
    __( 'Hillo forms', 'hillo' ),
    __( 'Hillo forms', 'hillo' ),
    'edit_posts',
    'hillo',
    'hillo_list_forms',
    'dashicons-groups',
    6
  );

  add_submenu_page(
    'hillo',
    __('Hillo settings'),
    __('Hillo settings'),
    'edit_posts',
    'hillo_settings',
    'hillo_settings_page_handler'
  );
} );

function hillo_settings_page_handler () {
  global $wpdb;
  $table_offers_name = $wpdb->prefix . 'hillo_offers';
  $offers = $wpdb->get_results( "SELECT * FROM {$table_offers_name}" );

  $table_prices_name = $wpdb->prefix . 'hillo_prices';
  $prices = $wpdb->get_results( "SELECT * FROM {$table_prices_name}" );

  // Include a react backend app
?>
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="hillo-admin"></div>
  </body>

<?php
}

// Output the database
function hillo_list_forms () {
  global $wpdb;
  $table_name = $wpdb->prefix . 'hillo_forms';
  $res = $wpdb->get_results( "SELECT * FROM {$table_name}" );

?>
  <div>
    <h2>Submitted forms</h2>
    <table class="wp-list-table widefat fixed striped table-view-list">
      <tr>
        <td>AVG purchase</td>
        <td>Visitors / m</td>
        <td>Conv %</td>
        <td>Improvement %</td>
        <td>Email</td>
        <td>Name</td>
        <td>Website</td>
      </tr>
    <?php foreach ($res as $row): ?>
      <?php /* TODO website should be a link with no tracking */ ?>
      <?php /* TODO visitors should have spaces for thousands and per month*/ ?>
      <tr>
        <td><?= $row->avg_purchase; ?> €</td>
        <td><?= number_format($row->visitors, 0, ',', ' '); ?></td>
        <td><?= $row->conversion; ?> %</td>
        <td><?= $row->improvement; ?> %</td>
        <td><?= $row->email; ?></td>
        <td><?= $row->name; ?></td>
        <td><?= $row->website; ?></td>
      </tr>
    <?php endforeach; ?>
    </table>
  </div>
<?php
}

/*"***************************** REST ****************************************/
function hillo_get_prices () {
  global $wpdb;
	$table_name = $wpdb->prefix . 'hillo_prices';
  $data = $wpdb->get_results( "SELECT * FROM {$table_name}" );

  if ($data === false) {
    return new WP_Error( 'hillo: get prices', __( 'Database query failed.', 'hillo' ) );
  } else {
    return rest_ensure_response( $data );
  }
}

function hillo_get_offers (WP_REST_Request $request) {
  global $wpdb;
	$table_name = $wpdb->prefix . 'hillo_offers';
  $data = $wpdb->get_results( "SELECT * FROM {$table_name}" );

  if ($data === false) {
    return new WP_Error( 'hillo: get offers', __( 'Database query failed.', 'hillo' ) );
  } else {
    return rest_ensure_response( $data );
  }
}

// TODO need to include a viewer for all submissiosns in the database
function hillo_get_forms (WP_REST_Request $request) {
  global $wpdb;
	$table_name = $wpdb->prefix . 'hillo_forms';

  $data = $wpdb->get_results( "SELECT * FROM {$table_name}" );

  if ($data === false) {
    return new WP_Error( 'hillo: get forms', __( 'Database query failed.', 'hillo' ) );
  } else {
    return new WP_REST_Response( $data );
  }
}

function hillo_post_form (WP_REST_Request $request) {
  $website = $request->get_param( 'website' );
  $email = $request->get_param( 'email' );
  $name = $request->get_param( 'name' );
  $avgPurchase = $request->get_param( 'avgPurchase' );
  $visitors = $request->get_param( 'visitors' );
  $convPercentage = $request->get_param( 'convPercentage' );
  $improvement = $request->get_param( 'improvement' );
  // TODO sanitise and check the parameters
  //  email and url validation
  //  avgPurchase, visitors > 0
  //  convPercentage and improvement [0, 100]

  global $wpdb;
	$table_name = $wpdb->prefix . 'hillo_forms';
  $result = $wpdb->insert( $table_name, [
      'avg_purchase' => $avgPurchase,   // INT
      'conversion' => $convPercentage,  // INT
      'improvement' => $improvement,    // INT
      'visitors' => $visitors,          // VARCHAR(255)
      'email' => $email,                // VARCHAR(255)
      'name' => $name,                  // VARCHAR(255)
      'website' => $website,            // VARCHAR(255)
    ]
  );

  if ($result === false) {
    return new WP_Error( 'hillo: post form', __( 'Failed to post to database', 'hillo' ) );
  } else {
    return new WP_REST_Response( [
      'status' => 201,
      'response' => 'Posted a form'
    ] );
  }
}

add_action( 'rest_api_init', function () {
  // getter is blocked for unauthorized users because it contains personal info
  register_rest_route( 'hillo/v1', '/forms', array(
    array(
      'methods' => 'GET',
      'callback' => 'hillo_get_forms',
      'permission_callback' => function () {
        return current_user_can( 'edit_posts' );
      },
    ),
    array(
      'methods' => 'POST',
      'callback' => 'hillo_post_form',
      'permission_callback' => '__return_true',
    ),
  ) );

  // /offers REST endpoints
  register_rest_route( 'hillo/v1', '/offers', array(
    array (
      'methods' => WP_REST_Server::READABLE,
      'callback' => 'hillo_get_offers',
      'permission_callback' => '__return_true',
    ),
    array (
      'methods' => WP_REST_Server::CREATABLE,
      'callback' => 'hillo_create_offer',
      'permission_callback' => function () {
        return current_user_can( 'edit_posts' );
      },
    ),
  ) );

  register_rest_route( 'hillo/v1', '/offers/(?P<id>[\d]+)', array(
    array (
      'methods' => WP_REST_Server::READABLE,
      'callback' => 'hillo_get_offer',
      'permission_callback' => '__return_true',
      'args' => array(
        'id' => array(
          'validate_callback' => function($param, $request, $key) {
            return is_numeric( $param );
          }
        ),
      ),
    ),
    array (
      'methods' => WP_REST_Server::EDITABLE,
      'callback' => 'hillo_update_offer',
      'permission_callback' => function () {
        return current_user_can( 'edit_posts' );
      },
    ),
    array(
      'methods' => WP_REST_Server::DELETABLE,
      'callback' => 'hillo_delete_offer',
      'permission_callback' => function () {
        return current_user_can( 'delete_posts' );
      },
    ),
  ) );

  // /prices REST endpoints
  register_rest_route( 'hillo/v1', '/prices', array(
    array (
      'methods' => WP_REST_Server::READABLE,
      'callback' => 'hillo_get_prices',
      'permission_callback' => '__return_true',
    ),
    array (
      'methods' => WP_REST_Server::CREATABLE,
      'callback' => 'hillo_create_price',
      'permission_callback' => function () {
        return current_user_can( 'edit_posts' );
      },
    ),
  ) );

  // TODO add parameter validation to all
  register_rest_route( 'hillo/v1', '/prices/(?P<id>[\d]+)', array(
    array(
      'methods' => WP_REST_Server::READABLE,
      'callback' => 'hillo_get_price',
      'permission_callback' => '__return_true',
      'args' => array(
        'id' => array(
          'validate_callback' => function($param, $request, $key) {
            return is_numeric( $param );
          }
        ),
      ),
    ),
    array(
      'methods' => WP_REST_Server::EDITABLE,
      'callback' => 'hillo_update_price',
      'permission_callback' => function () {
        return current_user_can( 'edit_posts' );
      },
    ),
    array(
      'methods' => WP_REST_Server::DELETABLE,
      'callback' => 'hillo_delete_price',
      'permission_callback' => function () {
        return current_user_can( 'delete_posts' );
      },
    ),
  ) );
} );

function hillo_get_price (WP_REST_Request $request) {
  $id = intval($request['id']);

  global $wpdb;
  $table_name = $wpdb->prefix . 'hillo_prices';
  $data = $wpdb->get_results(
    $wpdb->prepare( "SELECT * FROM {$table_name} WHERE id = %d", $id )
  );

  if (!$id || !($id > 0)) {
    return new WP_Error( 'hillo: get price', __( 'get price: invalid id', 'hillo' ) );
  } else if (!$data) {
    return new WP_Error( 'hillo: get price', __( 'database query failed', 'hillo' ) );
  } else {
    return rest_ensure_response( $data );
  }
}

// Returns 0 in the data if row was not changed, 1 if it was
function hillo_update_price (WP_REST_Request $request) {
  $id = intval($request['id']);

  $update_arr = [
    'max_revenue' => intval($request['max_revenue']),
    'price_per_test' => intval($request['price_per_test']),
    'n_tests' => intval($request['n_tests']),
  ];

  global $wpdb;
  $table_name = $wpdb->prefix . 'hillo_prices';
  $data = $wpdb->update( $table_name, $update_arr, ['id' => $id] );

  if (!$id || !($id > 0)) {
    return new WP_Error( 'hillo: update price', __( 'get price: invalid id', 'hillo' ) );
  } else if (false === $data) {
    return new WP_Error( 'hillo: update price', __( 'database query failed', 'hillo' ) );
  } else {
    return rest_ensure_response( $data );
  }
}

function hillo_delete_price (WP_REST_Request $request) {
  $id = intval($request['id']);

  global $wpdb;
  $table_name = $wpdb->prefix . 'hillo_prices';
  $res = $wpdb->delete( $table_name, [ 'id' =>  $id] );

  if (false === $res) {
    return new WP_Error( 'hillo: delete price', __( 'database delete failed', 'hillo' ) );
  } else {
    return new WP_REST_Response( true, 200 );
  }
}

function hillo_create_price (WP_REST_Request $request) {
  $arr = [
    'max_revenue' => intval($request['max_revenue']),
    'price_per_test' => intval($request['price_per_test']),
    'n_tests' => intval($request['n_tests']),
  ];

  global $wpdb;
  $table_name = $wpdb->prefix . 'hillo_prices';
  $data = $wpdb->insert( $table_name, $arr, ['%s', '%d'] );

  if (false === $data) {
    return new WP_Error( 'hillo: create price', __( 'database query failed', 'hillo' ) );
  } else {
    $id = $wpdb->insert_id;
    return new WP_REST_Response( $id, 200 );
  }
}

function hillo_create_offer (WP_REST_Request $request) {
  $arr = [
    'length' => intval($request['length']),
    'discount' => intval($request['discount']),
    'active' => intval($request['active']),
  ];

  global $wpdb;
  $table_name = $wpdb->prefix . 'hillo_offers';
  $data = $wpdb->insert( $table_name, $arr, ['%s', '%d'] );

  if (false === $data) {
    return new WP_Error( 'hillo: create price', __( 'database query failed', 'hillo' ) );
  } else {
    $id = $wpdb->insert_id;
    return new WP_REST_Response( $id, 200 );
  }
}

function hillo_get_offer (WP_REST_Request $request) {
  $id = intval($request['id']);

  global $wpdb;
  $table_name = $wpdb->prefix . 'hillo_offers';
  $data = $wpdb->get_results(
    $wpdb->prepare( "SELECT * FROM {$table_name} WHERE id = %d", $id )
  );

  if (!$id || !($id > 0)) {
    return new WP_Error( 'hillo: get offer', __( 'invalid id', 'hillo' ) );
  } else if (!$data) {
    return new WP_Error( 'hillo: get offer', __( 'database query failed', 'hillo' ) );
  } else {
    return rest_ensure_response( $data );
  }
}

// Returns 0 in the data if row was not changed, 1 if it was
function hillo_update_offer (WP_REST_Request $request) {
  $id = intval($request['id']);

  $update_arr = [
    'length' => intval($request['length']),
    'discount' => intval($request['discount']),
    'active' => intval($request['active']),
  ];

  global $wpdb;
  $table_name = $wpdb->prefix . 'hillo_offers';
  $data = $wpdb->update( $table_name, $update_arr, ['id' => $id] );

  if (!$id || !($id > 0)) {
    return new WP_Error( 'hillo: update offer', __( 'invalid id', 'hillo' ) );
  } else if (false === $data) {
    return new WP_Error( 'hillo: update offer', __( 'database query failed', 'hillo' ) );
  } else {
    return rest_ensure_response( $data );
  }
}

function hillo_delete_offer (WP_REST_Request $request) {
  $id = intval($request['id']);

  global $wpdb;
  $table_name = $wpdb->prefix . 'hillo_offers';
  $res = $wpdb->delete( $table_name, ['id' =>  $id] );

  if (false === $res) {
    return new WP_Error( 'hillo: delete offer', __( 'database delete failed', 'hillo' ) );
  } else {
    return new WP_REST_Response( true, 200 );
  }
}

// Database
function hillo_create_db() {
  global $wpdb;
  $charset_collate = $wpdb->get_charset_collate();

  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

  $table_name = $wpdb->prefix . 'hillo_forms';
  $sql = "CREATE TABLE $table_name (
    id INT AUTO_INCREMENT PRIMARY KEY,
    avg_purchase INT,
    conversion INT,
    improvement INT,
    visitors INT,
    email VARCHAR(255),
    name VARCHAR(255),
    website VARCHAR(255)
  ) $charset_collate;";
  dbDelta( $sql );

  $price_table_name = $wpdb->prefix . 'hillo_prices';
  $sql = "CREATE TABLE $price_table_name (
    id INT AUTO_INCREMENT PRIMARY KEY,
    max_revenue INT,
    price_per_test INT,
    n_tests INT
  ) $charset_collate;";
  dbDelta( $sql );

  $offer_table_name = $wpdb->prefix . 'hillo_offers';
  $sql = "CREATE TABLE $offer_table_name (
    id INT AUTO_INCREMENT PRIMARY KEY,
    length INT,
    discount INT,
    active BOOL
  ) $charset_collate;";
  dbDelta( $sql );
}
register_activation_hook( __FILE__, 'hillo_create_db' );
