module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#157AC8',
      },
    },
  },
  plugins: [
    require("@tailwindcss/typography"),
  ],
}
