import React from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import App from './App';
import AdminApp from './AdminApp';
import reportWebVitals from './reportWebVitals';

const container = document.getElementById('hillo-calculator');
if (container) {
  const root = createRoot(container);
  root.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>
  );
}
const admin_container = document.getElementById('hillo-admin');
if (admin_container) {
  const root = createRoot(admin_container);
  root.render(
    <React.StrictMode>
      <AdminApp />
    </React.StrictMode>
  );
}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
