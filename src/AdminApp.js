import React from "react"

import './index.css'
import PriceSettings from './components/PriceSettings'
import OfferSettings from './components/OfferSettings'

function AdminApp() {
  return (
    <div className="mt-8 mx-4 prose">
      <h2>Settings</h2>
      <PriceSettings />
      <OfferSettings />
    </div>
  )
}

export default AdminApp
