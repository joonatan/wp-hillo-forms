import './index.css'

import Calculator from './components/Calculator'

function App() {
  return (
    <div className="mt-8">
      <Calculator />
    </div>
  )
}

export default App
