import React from "react"

import Input from './Input'
import instance from '../api'

const OfferRow = ({
  id,
  length,
  discount,
  active,
  handleOfferLengthChange,
  handleOfferDiscountChange,
  handleOfferActiveChange,
  handleOfferDelete,
}) => (
  <tr>
    <td className="w-1/3">
      <Input
        val={length}
        onAccept={(value) => handleOfferLengthChange(id, value)}
        after="months"
        className="w-10"
      />
    </td>
    <td>
      <Input
        val={discount}
        onAccept={(value) => handleOfferDiscountChange(id, value)}
        after="%"
        className="w-10"
      />
    </td>
    <td>
      <input
        checked={active}
        onChange={(evt) => handleOfferActiveChange(id)}
        type="checkbox"
      />
    </td>
    <td className="text-red-500 text-center">
      <button onClick={() => handleOfferDelete(id)}>X</button>
    </td>
  </tr>
)

const OfferSettings = () => {
  const [offers, setOffers] = React.useState([])

  React.useEffect(() => {
    instance.get(`/offers`)
      .then((resp) => {
        setOffers(resp.data.map((x) => ({
          id: +x.id,
          length: +x.length,
          discount: +x.discount,
          active: (+x.active) !== 0,
        })))
      })
      .catch((err) => {
        console.error('ERROR: GET offer endpoint ', err)
      })
  }, [])

  const updateRemoteOffer = (id, data) => {
    instance.put(`/offers/${id}`, data)
      .then((resp) => {
        // TODO check for 0 and 1 and print whether the row was changed or not
        console.log('succesful offer update:', resp.data)
      })
      .catch((err) => {
        console.error('ERROR: PUT offer endpoint ', err)
      })
  }

  const handleOfferLengthChange = (id, val) => {
    const foffers = offers.filter((x) => x.id === id)
    if (foffers.length > 0) {
      setOffers(offers.map((x) => (
        x.id === id ? { ...x, length: val } : x
      )))
      updateRemoteOffer( id, { ...foffers[0], length: val } )
    }
  }

  const handleOfferDiscountChange = (id, val) => {
    const foffers = offers.filter((x) => x.id === id)
    if (foffers.length > 0) {
      setOffers(offers.map((x) => (
        x.id === id ? { ...x, discount: val } : x
      )))
      updateRemoteOffer( id, { ...foffers[0], discount: val } )
    }
  }

  const handleOfferActiveChange = (id) => {
    const foffers = offers.filter((x) => x.id === id)
    if (foffers.length > 0) {
      setOffers(offers.map((x) => (
        x.id === id ? { ...x, active: !x.active } : x
      )))
      updateRemoteOffer( id, { ...foffers[0], active: !foffers[0].active } )
    }
  }

  const handleOfferDelete = (id) => {
    // TODO confirm that the user wants to delete it

    // delete from database and then locally
    instance.delete(`/offers/${id}`)
      .then((resp) => {
        if (resp.data) {
          setOffers(offers.filter((x) => x.id !== id))
        } else {
          console.error('Delete failed')
        }
      })
      .catch((err) => {
        console.error( `ERROR: DELETE ${id} order failed with
          status "${err.response.status}"
          with message ${err.response.data.message}
        `)
      })
  }

  const handleOfferAdd = () => {
    instance.post(`/offers/`)
      .then((resp) => {
        console.log('succesful add row with retval = ', resp.data);
        const id = resp.data

        if (id > 0) {
          setOffers([...offers, { id: id, length: 0, discount: 0, active: false }])
        } else {
          console.error('create offer failed')
        }
      })
      .catch((err) => {
        console.error( `ERROR: POST price failed with
          status "${err.response.status}"
          with message ${err.response.data.message}
        `)
      })
  }

  return (
    <>
      <h3>Offer</h3>
      <table className="my-0">
        <thead>
          <tr>
            <td>Length</td>
            <td>Discount</td>
            <td>Active</td>
            <td className="text-right">Delete</td>
          </tr>
        </thead>
        <tbody>
          {offers.map((row) => (
            <OfferRow
              key={row.id}
              id={row.id}
              length={row.length}
              discount={row.discount}
              active={row.active}
              handleOfferLengthChange={handleOfferLengthChange}
              handleOfferDiscountChange={handleOfferDiscountChange}
              handleOfferActiveChange={handleOfferActiveChange}
              handleOfferDelete={handleOfferDelete}
            />
          ))}
        </tbody>
      </table>
      <div className="w-full">
        <div className="flex justify-around">
          <button
            aria-label="Add new"
            className="rounded-full bg-green-600 px-4 text-neutral-100"
            onClick={() => handleOfferAdd()}
          >+</button>
        </div>
      </div>
    </>
  )
}

export default OfferSettings
