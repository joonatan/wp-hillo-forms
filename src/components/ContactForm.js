import React from "react"

import InputField from "./InputField"

const ContactForm = ({ onSubmit }) => {
  const [email, setEmailImpl] = React.useState("")
  const [name, setNameImpl] = React.useState("")
  const [website, setWebsiteImpl] = React.useState("")

  const t = (str) => str

  const setEmail = (evt) => {
    setEmailImpl(evt.target.value)
  }
  const setName = (evt) => {
    setNameImpl(evt.target.value)
  }
  const setWebsite = (evt) => {
    setWebsiteImpl(evt.target.value)
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    onSubmit({
      email: email,
      name: name,
      website: website,
    })
  }

  // Partial form
  return (
    <div>
      <InputField
        name={t("Nimi")}
        type="text"
        value={name}
        onChange={setName}
        required={true}
      />
      <InputField
        name={t("Sähköposti")}
        type="email"
        value={email}
        onChange={setEmail}
        required={true}
      />
      <InputField
        name={t("Nettisivu")}
        type="text"
        value={website}
        onChange={setWebsite}
        required={true}
      />
      <div className="my-4">
        <button
          className="btn-cta text-2xl px-8 uppercase text-primary"
          type="submit"
          onClick={handleSubmit}
        >
          {t("Lähetä")}
        </button>
      </div>
    </div>
  )
}

export default ContactForm
