import React from "react"

import Slider from "rc-slider"
import 'rc-slider/assets/index.css'

import InputField from "./InputField"
import ContactForm from "./ContactForm"
import instance from '../api'
import Alert from "./Alert"

// TODO show errors till all fields are filled
// TODO make it clear that this is annual (12 month calculation)
// TODO add light gray background to every other element
const Result = ({
  visitors,
  avgPurchase,
  convPercentage,
  priceMonthly,
  improvement,
}) => {
  const findPrice = (sales) => {
    // TODO sort the priceMontly list (we use the first one as cheapest option)
    // and the last one if the revenue goes over
    const f_price = priceMonthly.filter((x) => sales < x.max_revenue)
    if (f_price.length > 0) {
      return f_price[0].price_per_test * f_price[0].n_tests
    } else {
      const p = priceMonthly[priceMonthly.length -1]
      return p.price_per_test * p.n_tests
    }
  }

  const sales = avgPurchase * visitors * (convPercentage / 100) * 12;
  const convOptim = [...Array(12)].reduce((agr, x) => (
    [...agr,
      (agr.length > 0
        ? agr[agr.length -1]
        : convPercentage
      ) * (improvement / 100 + 1)
    ]
  ), [])
  const salesOptim = convOptim.reduce((agr, x) => (
    agr + avgPurchase * visitors * (x / 100)
  ), 0)
  const difference = salesOptim - sales
  const pricePerMonth = findPrice(sales)
  const price = pricePerMonth * 12
  const proffit = difference - price
  const toMoney = (num) =>
    num.toLocaleString('fi-FI', { style: 'currency', currency: 'EUR' })

  const nextYearSales = convOptim[convOptim.length - 1] / 100 * 12 * avgPurchase * visitors

  return (
    <div className="mx-10 mb-8 border border-gray-500 px-2">
      <div className="flex justify-between">
        <div className="">Nykyinen liikevaihto</div>
        <div className="text-right">{toMoney(sales)}</div>
      </div>
      <div className="flex justify-between">
        <div className="">Seuraavan 12kk liikevaihto</div>
        <div className="text-right">{toMoney(salesOptim)}</div>
      </div>
      <div className="flex justify-between">
        <div className="font-semibold text-lg">Lisää liikevaihtoa</div>
        <div className="text-right font-semibold text-lg">{toMoney(difference)}</div>
      </div>
      <div className="flex justify-between">
        <div className="">Seuraavan vuoden liikevaihto</div>
        <div className="text-right">{toMoney(nextYearSales)}</div>
      </div>
      <div className="mt-4 flex justify-between">
        <div className="">Konversioprosentti vuoden päästä</div>
        <div className="text-right">{(convOptim[11]).toFixed(2)} %</div>
      </div>
      <div className="flex justify-between">
        <div className="">Optimoinnin kustannus (kuukaudessa) arvio</div>
        <div className="text-right">-{toMoney(pricePerMonth)}</div>
      </div>
      <div className="flex justify-between">
        <div className="">Erotus optimoinnin jälkeen</div>
        <div className="text-right">{toMoney(proffit)}</div>
      </div>
    </div>
  )
}

// TODO reasonable defaults or show empty results till all fields are answered
const Calculator = () => {
  const [submitted, setSubmitted] = React.useState(false)
  const [avgPurchase, setAvgPurchase] = React.useState(0)
  const [visitors, setVisitors] = React.useState(0)
  const [convPercentage, setConvPercentage] = React.useState(0)
  const [improvement, setImprovement] = React.useState(3)
  const [offer, setOffer] = React.useState({ discount: 20, length: 0 })
  const [priceMonthly, setPriceMonthly] = React.useState([
    {
      maxRevenue: 250000,
      pricePerTest: 600,
      nTests: 0,
    },
  ])
  const [errorMsg, setErrorMsg] = React.useState('')

  // Retrieve price list from REST
  React.useEffect(() => {
    instance.get(`/prices`)
      .then((resp) => {
        setPriceMonthly(resp.data)
      })
      .catch((err) => {
        console.error('ERROR: GET prices endpoint ', err)
      })
  }, [])

  React.useEffect(() => {
    instance.get(`/offers`)
      .then((resp) => {
        const d = resp.data.filter((x) => (+x.active) !== 0)
        if (d.length > 0) {
          // Take first offer
          setOffer(d[0])
        }
      })
      .catch((err) => {
        console.error('ERROR: GET offer endpoint ', err)
      })
  }, [])

  // Post the form using REST
  const handleSubmit = (data) => {
    // TODO validate form (all fields needs to be answered for submit to work)

    if (data.email === ''
      || data.name === ''
      || data.website === ''
      || avgPurchase === 0
      || visitors === 0
      || convPercentage === 0
      || improvement === 0
    ) {
      console.error('missing form inputs')
      setErrorMsg('missing form input')
      // TODO show which of the fields are missing
    } else {
      const d = {
        ...data,
        avgPurchase: avgPurchase,
        visitors: visitors,
        convPercentage: convPercentage,
        improvement: improvement,
      }

      instance.post(`/forms`, d)
        .then((resp) => {
          setSubmitted(true)
        })
        .catch((err) => {
          // TODO alert
          console.error('ERROR: POST forms endpoint ', err)
        })
    }
  }

  // TODO format offer nicely and overlay on the calculations not in the bottom
  return submitted ? (
    <div className="prose mx-auto bg-green-600 text-white rounded-md">
      <div className="mx-4 p-2">Lähetetty</div>
    </div>
  ) : (
    <form
      className="prose mx-auto relative"
      method="post"
      name="contact"
    >
      {errorMsg !== '' && (
        <Alert level="error" message={errorMsg} onClose={() => setErrorMsg('')} />
      )}
      <input type="hidden" name="form-name" value="contact" />
      <p className="hidden">
        <label>
          Don’t fill this out if you’re human:
          <input type="hidden" name="bot-field" />
        </label>
      </p>
      <InputField
        name="Keskiostos (€)"
        type="number"
        value={avgPurchase}
        minValue={0}
        onChange={(evt) => setAvgPurchase(evt.target.value)}
      />
      <InputField
        name="Kävijämäärä (kuukaudessa)"
        type="number"
        value={visitors}
        minValue={0}
        onChange={(evt) => setVisitors(evt.target.value)}
      />
      <InputField
        name="Konversio prosentti (%)"
        type="number"
        value={convPercentage}
        maxValue={5}
        minValue={0}
        step={0.1}
        onChange={(evt) => setConvPercentage(evt.target.value)}
      />
      <div className="mb-20 w-full">
        <label id="slider-label mb-4">
          Kuukausittainen parannus (%)
        </label>
        <Slider
          max={5}
          min={0}
          step={0.1}
          onChange={setImprovement}
          value={improvement}
          marks={{ 0: 0, 1: "1%", 2: "2%", 3: "3%", 4: "4%", 5: "5%" }}
        />
      </div>
      <Result
        visitors={visitors}
        avgPurchase={avgPurchase}
        convPercentage={convPercentage}
        priceMonthly={priceMonthly}
        improvement={improvement}
      />
      <ContactForm onSubmit={handleSubmit} />
      {offer.length > 0 && (
        <div>
          <span className="uppercase">Tarjous: </span>
          -{offer.discount} % ekat {offer.length} kuukautta.
        </div>
      )}
    </form>
  )
}

export default Calculator
