import React from "react"

const InputField = ({
  name,
  type,
  value,
  maxValue,
  minValue,
  step,
  onChange,
  validationErrorMsg,
  required,
}) => (
  <div className="group relative z-0 mb-6 w-full text-neutral-500">
    {type !== "textarea" ? (
      <input
        className="peer block w-full border-0 border-b-2 border-gray-300 bg-transparent py-2 px-0 text-sm outline-none focus-visible:outline-none"
        type={type}
        name={name}
        max={maxValue}
        min={minValue}
        step={step}
        onChange={onChange}
        value={value}
        placeholder=" "
      />
    ) : (
      <textarea
        className="peer block w-full border-0 border-b-2 border-gray-300 bg-transparent py-2 px-0 text-sm outline-none"
        name={name}
        value={value}
        onChange={onChange}
      />
    )}
    <label
      htmlFor={name}
      className="absolute top-3 -z-10 -translate-y-6 transform text-sm duration-300 peer-placeholder-shown:translate-y-0 peer-focus:left-0 peer-focus:-translate-y-6 peer-focus:text-blue-600"
    >
      <span
        className={
          required
            ? "block text-sm after:ml-2 after:text-red-500 after:content-['*']"
            : ""
        }
      >
        {name}
      </span>
    </label>
    <p className="invisible mt-2 text-sm text-pink-600 peer-invalid:visible">
      {validationErrorMsg}
    </p>
  </div>
)

export default InputField
