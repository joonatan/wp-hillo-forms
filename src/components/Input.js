import React from 'react'

const Input = ({ val, onAccept, className, after }) => {
  const [value, setValue] = React.useState(val)

  const onChange = (evt) => {
    setValue(evt.target.value)
  }

  // TODO is blur good enough here?
  const onBlur = (evt) => {
    //console.log('on blur: ', evt)
    onAccept(evt.target.value)
  }

  return (
    <>
      <input
        onChange={onChange}
        onBlur={onBlur}
        className={className}
        value={value}
      />
      {after}
    </>
  )
}

export default Input
