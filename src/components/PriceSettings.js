import React from 'react'

import Input from './Input'
import instance from '../api'

const PriceRow = ({
  id,
  max_revenue,
  price_per_test,
  n_tests,
  handlePriceMaxRevenueChange,
  handlePricePerTestChange,
  handleNTestsChange,
  handlePriceDelete,
}) => {
  return (
    <tr>
      <td>
        <Input
          val={max_revenue}
          onAccept={(value) => handlePriceMaxRevenueChange(id, value)}
          after="€"
          className="w-20"
        />
      </td>
      <td>
        <Input
          val={price_per_test}
          onAccept={(value) => handlePricePerTestChange(id, value)}
          after="€"
          className="w-10"
        />
      </td>
      <td>
        <Input
          val={n_tests}
          onAccept={(value) => handleNTestsChange(id, value)}
          after=""
          className="w-10"
        />
      </td>
      <td className="text-red-500">
        <button onClick={() => handlePriceDelete(id)}>X</button>
      </td>
    </tr>
  )
}

const PriceSettings = () => {
  const [prices, setPrices] = React.useState([])

  // TODO sort the price list based on max_revenue

  // Retrieve price list from REST
  React.useEffect(() => {
    instance.get(`/prices`)
      .then((resp) => {
        setPrices(resp.data.map((x) => ({
          id: +x.id,
          max_revenue: +x.max_revenue,
          price_per_test: +x.price_per_test,
          n_tests: +x.n_tests,
        })))
      })
      .catch((err) => {
        console.error('ERROR: GET prices endpoint ', err)
      })
  }, [])

  const updateRemotePrice = (id, data) => {
    instance.put(`/prices/${id}`, data)
      .then((resp) => {
        // TODO check for 0 and 1 and print whether the row was changed or not
        console.log('succesful price update:', resp.data)
      })
      .catch((err) => {
        console.error('ERROR: PUT price endpoint ', err)
      })
  }

  // these are called when the user accepts the input
  const handlePriceMaxRevenueChange = (id, val) => {
    // TODO it's also bad because it modifies the whole array
    const fprices = prices.filter((x) => x.id === id)
    if (fprices.length > 0) {
      // update local
      setPrices(prices.map((x) => (
        x.id === id ? { ...x, max_revenue: val } : x
      )))
      updateRemotePrice( id, { ...fprices[0], max_revenue: val } )
    }
  }

  const handlePricePerTestChange = (id, val) => {
    const fprices = prices.filter((x) => x.id === id)
    if (fprices.length > 0) {
      setPrices(prices.map((x) => (
        x.id === id ? { ...x, price_per_test: val } : x
      )))
      updateRemotePrice( id, { ...fprices[0], price_per_test: val } )
    }
  }

  const handleNTestsChange = (id, val) => {
    const fprices = prices.filter((x) => x.id === id)
    if (fprices.length > 0) {
      setPrices(prices.map((x) => (
        x.id === id ? { ...x, n_tests: val } : x
      )))
      updateRemotePrice( id, { ...fprices[0], n_tests: val } )
    }
  }

  const handlePriceDelete = (id) => {
    // TODO confirm that the user wants to delete it

    // delete from database and then locally
    instance.delete(`/prices/${id}`)
      .then((resp) => {
        if (resp.data) {
          setPrices(prices.filter((x) => x.id !== id))
        } else {
          console.error('Delete failed')
        }
      })
      .catch((err) => {
        console.error( `ERROR: DELETE ${id} price failed with
          status "${err.response.status}"
          with message ${err.response.data.message}
        `)
      })
  }

  // TODO this should allow canceling the addition
  // that is it should first show the new line and only add if the user doesn't cancel the addition
  // this allows us to POST the row instantly instead of adding + saving the data
  // this would also avoid accidentially adding a row that is empty
  const handlePriceAdd = () => {
    instance.post(`/prices/`)
      .then((resp) => {
        console.log('succesful add row with retval = ', resp.data);
        const id = resp.data

        if (id > 0) {
          setPrices([...prices, { id: id, max_revenue: 0, price_per_test: 0, n_tests: 0 }])
        } else {
          console.error('create offer failed')
        }
      })
      .catch((err) => {
        console.error( `ERROR: POST price failed with
          status "${err.response.status}"
          with message ${err.response.data.message}
        `)
      })
  }

  return (
    <>
      <h3>Prices</h3>
      <table className="my-0">
        <thead>
          <tr>
            <td>Max revenue</td>
            <td>Price per test</td>
            <td>Number of tests</td>
            <td>Delete</td>
          </tr>
        </thead>
        <tbody>
          {prices.map((row) => (
            <PriceRow
              key={row.id}
              id={row.id}
              max_revenue={row.max_revenue}
              price_per_test={row.price_per_test}
              n_tests={row.n_tests}
              handlePriceMaxRevenueChange={handlePriceMaxRevenueChange}
              handlePricePerTestChange={handlePricePerTestChange}
              handleNTestsChange={handleNTestsChange}
              handlePriceDelete={handlePriceDelete}
            />
          ))}
        </tbody>
      </table>
      <div className="w-full">
        <div className="flex justify-around">
          <button
            aria-label="Add new"
            className="rounded-full bg-green-600 px-4 text-neutral-100"
            onClick={() => handlePriceAdd()}
          >+</button>
        </div>
      </div>
    </>
  )
}

export default PriceSettings
