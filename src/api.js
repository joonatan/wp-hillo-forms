import axios from "axios"

// could use this instead to set headers and domain
const instance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  timeout: 5000,
  headers: window.data ? {
    'X-WP-Nonce': window.data.nonce,
  } : {},
})

export default instance
