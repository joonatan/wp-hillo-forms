# Combound Calculator for Wordpress
[Hillopurkki] (https://hillopurkki.fi) needed a combound calculator for cumulative sales optimisation.

Hillopurkki is running Wordpress.

The problem:

- Calculators that require POST calls for changing the values have really bad user experience.
- Wordpress natively doesn't support anything other than browser Javascript and jQuery.
- jQuery is slow, outdated, and hard to manage.

Solution:

- Add REST endpoints with custom MySQL datatables for persistent data
- Add bundled React front-end for the calculator

Bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Load the optimised bundle in WordPress and embed the widget using a shortcode.

Also add settings widget and submissions forms in the backend.

[Test it at](https://hillopurkki.fi/laskuri)

## Design
Persistent data like prices are stored in the backend.

After the user fills the calculator and fills contact information the information can be sent using
REST to backend.

Backend includes views for looking at the submitted forms and changing settings.

### User interface
![Front-end user interface](images/hillopurkki-calculator-front-cropped.png)

### Admin interface
![Backend settings user interface](images/hillopurkki-calculator-backend-settings-cropped.png)

![Backend form submissions user interface](images/hillopurkki-calculator-backend-submissions-cropped.png)
